## About 

---

This task was done using [webdriver.io](https://github.com/webdriverio/webdriverio), [cucumber.js](https://github.com/cucumber/cucumber-js) and page object pattern to increase readbility and maintainability 

```Scenario Outline``` was adopted to give room for several variations of search without duplicating steps.

Currently, the test is running only in google chrome.

### Installing

In order to execute chromedriver and/or selenium-standalone, it is necessary to install [Java](https://www.java.com/en/download/help/download_options.xml). 

Also, it is necessary to install [node.js](https://nodejs.org/en/) in order to execute npm commands.

To install dependencies:

* ```npm install```

### Running

* ```npm test```


### Code standarts

---

* [Prettier:](https://www.npmjs.com/package/prettier) Code formatter - ```npm run format```
* [Eslint:](https://eslint.org/) Code linting - ```npm run lint:fix```
* [Husky:](https://www.npmjs.com/package/husky) Git hooks to prevent bad commits. Currently using precommit hook

### Exposing feature scenarios

---

In order to expose the specified scenarios in a cleaner way, [featurebook-express](https://www.npmjs.com/package/feature-express) is avaliable:

* ```npm run featurebook```