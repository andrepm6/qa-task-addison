"use strict";

const page = require("./page");

class HomePage extends page {
  get bookingLogo() {
    return $("#logo_no_globe_new_logo");
  }
  get languageSelector() {
    return $("[data-id=language_selector]");
  }
  get currencySelector() {
    return $("[data-id=currency_selector]");
  }
  get currencyList() {
    return $("#current_currency_foldout");
  }
  get euro() {
    return $("[data-lang=EUR]");
  }
  get country() {
    return $("[data-lang=en-us]");
  }
  get destination() {
    return $("#ss");
  }
  get checkinButton() {
    return $("[data-mode=checkin]");
  }
  get checkoutButton() {
    return $("[data-mode=checkout]");
  }
  get guestsButton() {
    return $("#xp__guests__toggle");
  }
  get noAdults() {
    return $("#group_adults");
  }
  get noChildren() {
    return $("#group_children");
  }
  get childrenAge() {
    return $("[name=age]");
  }

  open(url) {
    browser.url(url);

    this.bookingLogo.waitForVisible(10000);

    return this;
  }

  setDestination(destination) {
    this.destination.setValue(destination);
  }

  setCheckinDate(initialDate) {
    this.checkinButton.click();

    if (initialDate == "last day of current month") {
      const elemen = $$("[class=c2-month]");

      const days = elemen[0].$$("[class=c2-day-inner]");

      days[days.length - 1].click();
    }

    return this;
  }

  setCheckoutDate(initialDate) {
    this.checkoutButton.click();

    if (initialDate == "first day of next month") {
      const elemen = $$("[class=c2-month]");

      const days = elemen[1].$$("[class=c2-day-inner]");

      days[0].click();
    }

    return this;
  }

  selectCurrency() {
    this.currencySelector.click();

    this.currencyList.waitForVisible(5000);

    this.euro.click();
  }

  selectCountry() {
    this.languageSelector.click();
    this.country.click();
  }

  setGuests(adults, children, age) {
    this.guestsButton.click();

    this.noAdults.selectByValue(adults);

    this.noChildren.selectByValue(children);

    this.childrenAge.selectByValue(age);
  }

  performSearch() {
    this.searchButton.click();

    this.searchResults.waitForVisible(10000);
  }
}

module.exports = new HomePage();
