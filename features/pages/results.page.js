"use strict";

const page = require("./page");

class ResultsPage extends page {
  get travelingWorkCheckbox() {
    return $("[name=sb_travel_purpose]");
  }
  get noRooms() {
    return $("#no_rooms");
  }
  get hotelsScore() {
    return $$("data-score");
  }

  setTravelingforWork() {
    this.travelingWorkCheckbox.click();

    return this;
  }

  setNumberRooms(rooms) {
    this.noRooms.selectByValue(rooms);

    return this;
  }

  findHotel(reviewMark, price) {
    const hotels = $$("[data-score]");

    for (let i = 0; i < hotels.length; i++) {
      let score = hotels[i].getAttribute("data-score");
      score = score.replace(",", ".");

      score = parseFloat(score);
      reviewMark = parseFloat(reviewMark);

      if (score > reviewMark) {
        let hotelName = hotels[i].$(".sr-hotel__name").getText();

        try {
          let hotelPrice = hotels[i]
            .$(".totalPrice.totalPrice_no-rack-rate.entire_row_clickable")
            .getText();

          hotelPrice = hotelPrice.split(" ");
          hotelPrice = hotelPrice[hotelPrice.length - 1];
          hotelPrice = parseFloat(hotelPrice);

          let hotelName = hotels[i].$(".sr-hotel__name").getText();

          if (hotelPrice < price) {
            console.log("The name of the hotel selected is: " + hotelName);

            break;
          }
        } catch (err) {
          console.log(hotelName + " is out of rooms for the selected dates");
        }
      }
    }
  }
}

module.exports = new ResultsPage();
