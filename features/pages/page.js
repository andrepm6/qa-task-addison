"use strict";

class Page {
  get searchButton() {
    return $("[type=submit]");
  }
  get searchResults() {
    return $("#searchresultsTmpl");
  }
}

module.exports = Page;
