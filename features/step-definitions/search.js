"use strict";

const { Given, When, Then } = require("cucumber");

const home = require("../pages/home.page");
const results = require("../pages/results.page");

Given("that the booking home page is being displayed", () => {
  home.open("https://www.booking.com/");
});

Given("that the language of the page is english-US", () => {
  home.selectCountry();
});

Given("the currency is euro", () => {
  home.selectCurrency();
});

When("the destination is {destination}", destination => {
  home.setDestination(destination);
});

When(
  "inform {initialDate} and {finalDate} as dates",
  (initialDate, finalDate) => {
    home.setCheckoutDate(finalDate).setCheckinDate(initialDate);
  }
);

When(
  "with {adults} adult and {children} {age} year old children as guests",
  (adults, children, age) => {
    home.setGuests(adults, children, age);
  }
);

Then("perform the search", () => {
  home.performSearch();
});

When(
  "refine the search with {noRooms} rooms and traveling for work",
  noRooms => {
    results.setTravelingforWork().setNumberRooms(noRooms);
  }
);

Then(
  "find a hotel with rate higher than {reviewScore} and price under {price}",
  (reviewScore, price) => {
    results.findHotel(reviewScore, price);
  }
);
