Feature: Search

In order to find a good hotel
As an ordinary user
I want to perform a search at booking.com page

Background:
    Given that the booking home page is being displayed
    When the currency is euro
    And that the language of the page is english-US
  
Scenario Outline: Perform a refined search
    When the destination is <destination>
    And inform <initial_date> and <final_date> as dates
    And with <adults> adult and <children> <age> year old children as guests
    Then perform the search
    And refine the search with <no_rooms> rooms and traveling for work
    And perform the search
    Then find a hotel with rate higher than <review_mark> and price under <price>

    Examples:
        |        destination       |            initial_date              |           final_date             |    adults   | children | age | no_rooms | review_mark |  price |
        | Málaga, Andalucía, Spain |     last day of current month        |      first day of next month     |      1      |     1    |  5  |     2    |    8.0      |   200  |
